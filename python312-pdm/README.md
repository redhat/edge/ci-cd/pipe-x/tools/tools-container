# Python 3.12 PDM

A container image based on UBI9 with [PDM][_pdm] installed for package management.

This image makes an additional Python 3.12 interpreter available to PDM-managed Python 3.12 
projects.

[_pdm]: https://pdm.fming.dev/latest/
