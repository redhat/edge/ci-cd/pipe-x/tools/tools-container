# Compose Promoter

A container image for promoting composes.

The container is based in ubi9 and contains the following extra tools:

- openssl
- krb5
- wget
