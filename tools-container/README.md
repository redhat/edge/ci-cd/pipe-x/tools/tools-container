# Tools Container

A container image for tools used at different jobs at the CI pipelines.

The container is based in ubi9 and contains the following extra tools:

- curl
- wget
- git
- jq
- aws-cli

