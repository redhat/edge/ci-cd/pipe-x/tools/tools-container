# Python3-Friends

A container image for running formatters and linters such as black to check
and enforce the formatting style of Python code.
Also for running tests with pytest and generate the code coverage.

The container is based in ubi9 and contains the following additions:

- python3-pip
- black
- flake8
- isort
- pdm
- pylint
- pytest
- pytest-cov
- mypy
- bandit
