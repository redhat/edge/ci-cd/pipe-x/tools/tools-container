# protoc-gen-doc Container

A container image based on ubi9 with the `protoc-gen-doc` binary, to
generate documentation for the Google Protocol Buffers compiler (protoc).

The container is based on the latest stable ubi9 image and it uses the latest
`protoc-gen-doc` binary from the official project here:

https://github.com/pseudomuto/protoc-gen-doc/releases
