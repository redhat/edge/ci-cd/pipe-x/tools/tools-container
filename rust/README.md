# Rust

A container image based on UBI9 that includes the Rust toolchain.

This containerfile supports multiple Rust versions through the RUST_VERSION build arg.

The auto-toolchain-containers project currently builds the following Rust versions:

* `1.79.0`
* `1.80.1`
* `1.81.0`
* `1.82.0`

## Support
These container images are not official Red Hat products and come as-is with no support.

For supported Red Hat builds of the Rust toolchain, please use the `cargo` distribution from 
official Red Hat DNF repositories.